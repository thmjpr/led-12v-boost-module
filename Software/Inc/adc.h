/**
  ******************************************************************************
  * File Name          : ADC.h
  * Description        : This file provides code for the configuration
  *                      of the ADC instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __adc_H
#define __adc_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern ADC_HandleTypeDef hadc;

/* USER CODE BEGIN Private defines */
	 
#define NUM_ADC_CHANNELS	5
#define NUM_ADC_AVGS		16		//maximum 16 averages
#define TEMP30_CAL_ADDR ((uint16_t*) ((uint32_t) 0x1FFFF7B8))	//Temperature sensor raw value at 30 degrees C, VDDA=3.3V
#define TEMP110_CAL_ADDR ((uint16_t*) ((uint32_t) 0x1FFFF7C2))	//Temperature sensor raw value at 110 degrees C, VDDA=3.3V
#define VREFINT_CAL_ADDR ((uint16_t*) ((uint32_t) 0x1FFFF7BA))	//Internal voltage reference raw value at 30 degrees C, VDDA=3.3V
#define ADC_FULL_SCALE	0xFFF

#define SUPPLY_VOLTAGE  3.30
	
	typedef enum
	{
		ADC_VSense,		 		    //= ADC_CHANNEL_0,	//since we set up the ADC in sequence, just use incremental enum
		ADC_VAmbient,		 		//= ADC_CHANNEL_1,
		ADC_USBV,		 		    // = ADC_CHANNEL_2,
		ADC_TEMP,		 		    // = ADC_CHANNEL_TEMPSENSOR
		ADC_INT_REF,		 		//= ADC_CHANNEL_VREFINT,
	}adc_ch;

	volatile bool adc_complete;
	
	
/* USER CODE END Private defines */

void MX_ADC_Init(void);

/* USER CODE BEGIN Prototypes */
	float get_led_voltage(void);
	float get_vcc_voltage(void);
	float get_temperature(void);
	uint16_t get_light_level(void);
	 
	void cal_vref_int(void);
	void start_adc(void);

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ adc_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
