/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#include <stdio.h>

#define MAX_LED_PERCENT	0.45		//scale brightness (for 4V vs 8V input etc.)		//for 4V setup?
//#define MAX_LED_PERCENT	0.5				//scale brightness (for 12V battery)
#define TIMER_MULTIPLIER 5.0			//



static uint32_t abs_diff_32(uint32_t number1, uint32_t number2)
{
	int64_t diff = (uint64_t)number1 - (uint64_t)number2;
		
	if (diff < 0)
		return (uint32_t)(-diff);
	else
		return (uint32_t)diff;
}
	
//Limit a signed integer to +/- limit number
//positive number only for constraint
static inline void constrain_pm(int32_t * number, int32_t constraint)
{
	//assert(constraint >= 0);
	if(*number > constraint)
		*number = constraint;
	else if(*number < -constraint)
		*number = -constraint;
	else
		return;
}
/*
static inline void constrain(int32_t * number, int32_t constraint_low, int32_t constraint_high)
{
	//assert(constraint >= 0);
	if(*number > constraint_high)
		*number = constraint_high;
	else if(*number < constraint_low)
		*number = constraint_low;
	else
		return;
}*/


static inline void constrain(float * number, float constraint_low, float constraint_high)
{
	//assert(constraint >= 0);
	if(*number > constraint_high)
		*number = constraint_high;
	else if(*number < constraint_low)
		*number = constraint_low;
	else
		return;
}


#define LEN(X) (sizeof(X)/sizeof(*X))
	


#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdbool.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
	void led_pwm(float percent);
	bool led_sts(bool led);
	void light_sensor(bool state);
	
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED_OP_V_Pin GPIO_PIN_0
#define LED_OP_V_GPIO_Port GPIOA
#define Ambient_V_Pin GPIO_PIN_1
#define Ambient_V_GPIO_Port GPIOA
#define USB_V_Pin GPIO_PIN_2
#define USB_V_GPIO_Port GPIOA
#define Button1_Pin GPIO_PIN_5
#define Button1_GPIO_Port GPIOA
#define Button1_EXTI_IRQn EXTI4_15_IRQn
#define Light_sense_en_Pin GPIO_PIN_6
#define Light_sense_en_GPIO_Port GPIOA
#define Mode_Pin GPIO_PIN_7
#define Mode_GPIO_Port GPIOA
#define LED_sts_Pin GPIO_PIN_1
#define LED_sts_GPIO_Port GPIOB
#define PWM1_Pin GPIO_PIN_9
#define PWM1_GPIO_Port GPIOA
#define PWM2_Pin GPIO_PIN_10
#define PWM2_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

#define green_led_state() HAL_GPIO_ReadPin(LED_sts_GPIO_Port, LED_sts_Pin)
#define ARR_LEN(x) (sizeof(x)/sizeof(*x))
	
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
