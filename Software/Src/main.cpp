/**
******************************************************************************
* @file           : main.c
* @brief          : Main LED program
******************************************************************************
* @attention
*
*
******************************************************************************
*/


/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "iwdg.h"
#include "tim.h"
#include "gpio.h"

#include "LED_pattern.h"


volatile bool button_pressed = false;
volatile bool led_update = false;

volatile uint32_t tick = 0;
LED_pattern led;
void error_flash(uint16_t delay);

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);


/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{

	/* MCU Configuration--------------------------------------------------------*/
	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_ADC_Init();
	MX_TIM1_Init();
	MX_TIM14_Init();
	MX_IWDG_Init();
	
	//Watchdog timeout 40kHz osc -> 40k/8/4095 = ~1s
	//if read ADC battery voltage, etc.
  
	light_sensor(true);                      //Enable light sensor
	start_adc();                             //Begin scanning adc
	HAL_TIM_Base_Start_IT(&htim14);       	 //Start the timer running
	__HAL_DBGMCU_FREEZE_IWDG();      		//Freeze the watchdog when debugging and paused
	HAL_Delay(100);							//Wait for ADC to update
	cal_vref_int();							//Calibrate internal vref
	
	if(get_vcc_voltage() < 2.90)			//Low battery voltage
	{
		error_flash(1500);
		//may want to sleep
	}

	
	led_sts(false);


	while(1)
	{	  
		//can run self test, if LED connected -> should see voltage spike up.
		//Voltage will be ~3V at idle, unless SRF is blown then it will be 0V
		
		//
		if(button_pressed)
		{
			led.next_pattern();			//increment
			button_pressed = false;
		}

		//Might need moving average as the voltage will spike during PWM and then drop after, if you want to modulate brightness 
		if(adc_complete)
		{
			float v = get_led_voltage();			//get LED output voltage
			uint16_t l = get_light_level();			//get light level
			
			if (get_temperature() > 50.0)			//High MCU temperature
			{
				error_flash(200);
			}
			
			//No light seems to be ~1.7V then full light will bring it to 3V
			if(get_vcc_voltage() < 2.90)		//Low battery
			{
				error_flash(1000);
			}
			
			if (get_led_voltage() > 20.0)		//Overvoltage, LED issue
			{
				error_flash(300);
			}
			
			HAL_IWDG_Refresh(&hiwdg);      		//Watchdog refresh
			adc_complete = false;      			//
		}
		
		//Update LED every 10ms
		if (led_update)
		{
			led_pwm(led.run());  				//get next brightness from pattern generator
			led_update = false;
		}
	  
		//could add sleep, would need to disable IWDG before entering sleep or it will wake up
	}

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Initializes the CPU, AHB and APB busses clocks 
	*/
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI | RCC_OSCILLATORTYPE_LSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL10;
	RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB busses clocks 
	*/
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
	                            | RCC_CLOCKTYPE_PCLK1;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
	{
		Error_Handler();
	}
}


//Adjust PWM to LED boost circuitry
//TIM1_CH2 is main PWM output
//PWM frequency ~160kHz
void led_pwm(float percent)
{
	//limit to 0-100% brightness
	constrain(&percent, 0, 100);
	
	//convert percent to 0-200 timer range, so 80% maximum PWM duty cycle (200/250).
	percent *= 2;
	percent = percent * MAX_LED_PERCENT;		//scale back for debugging or higher input voltage
	
	//__HAL_TIM_GET_AUTORELOAD(&htim1);							//gets the Period set for PWM
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (uint16_t)percent);      		//set PWM duty cycle
	
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);      			        //needed to enable timer
}

//Status LED
bool led_sts(bool state)
{
	static bool current;
	
	current = HAL_GPIO_ReadPin(LED_sts_GPIO_Port, LED_sts_Pin);
	
	if (state)
		HAL_GPIO_WritePin(LED_sts_GPIO_Port, LED_sts_Pin, GPIO_PIN_SET);
	else
	{
		HAL_GPIO_WritePin(LED_sts_GPIO_Port, LED_sts_Pin, GPIO_PIN_RESET);
	}
	
	return current;
}

//Enable light sensor
//when on = low, then wait some time and take a reading
void light_sensor(bool state)
{
	if (state)
		HAL_GPIO_WritePin(Light_sense_en_GPIO_Port, Light_sense_en_Pin, GPIO_PIN_SET);            //set open collector on
		else
	{
		HAL_GPIO_WritePin(Light_sense_en_GPIO_Port, Light_sense_en_Pin, GPIO_PIN_RESET);
	}
}


//--------------------------- Interrupts --------------------------

//External interrupt 
//button press
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	static uint32_t last_press = 0xFFFFFFFF;

	if (GPIO_Pin == Button1_Pin)
	{
		if (abs_diff_32(tick, last_press) > 4)     //if >400ms since last button press
			{
				button_pressed = true;
				last_press = tick;
			}
	}
}

//Timer (TIM14)
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if (htim == &htim14)        //TIM14 elapsed (40M / 400 / 1000 = 0.01s)
		{
			tick++;      				//increment 0.01s tick
			led_update = true;			//
		}
}


//-------------------------------------------------------------------
//Errors

//Infinite error loop
void error_flash(uint16_t delay)
{
	led_pwm(0);
	
	while (1)
	{
		led_sts(!led_sts(true));
		HAL_Delay(delay);
		HAL_IWDG_Refresh(&hiwdg);        		//Watchdog refresh
	}
}


/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
		/* User can add his own implementation to report the HAL error return state */
	while (1)
	{
		led_pwm(0);
		HAL_Delay(500);
		led_sts(!led_sts(true));
	}
	
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
	/* USER CODE BEGIN 6 */
		/* User can add his own implementation to report the file name and line number,
		tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
