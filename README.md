# 12V_LED_Boost_stm32f030

![2021-09-19_16.20.15](/uploads/1fa58b4edd08d7409043018abf68d253/2021-09-19_16.20.15.jpg)



## Synopsis

Board to convert single lithium cell voltage (~3-6V) to 12V to run LED strips, COB lights, etc.



## Hardware

- STM32F030F4P6 microcontroller
- SOT-23 N-FET
- SOD123 diode 
- 10uH inductor
- Generic li-ion charger board, USB micro or USB-C type, optional


## PCB
- Rev_1: Functional, add capacitor across LED output to dampen voltage spikes.
- Rev_2: Not built yet.


## Issues
- 


## License

MIT license where applicable.  